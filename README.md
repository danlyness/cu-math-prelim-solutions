# Qualifying Exam Soutions

This repository will *hopefully* host a set of clear solutions to the mathematics preliminary exams at [CU Boulder Mathematics Department](https://www.colorado.edu/math/). There are three topics covered: algebra, analysis, and topology/geometry.

The exams and syllabi can be referenced [here](https://www.colorado.edu/math/graduate-program/graduate-resources/preliminary-exams).

My hope in creating this repository is that it is collaborative: feel free to clone the project, make merge requests, flag issues, etc. For those new to git, there is plenty of great [documentation](https://git-scm.com/doc), and if you have any questions, feel free to shoot me a message.
